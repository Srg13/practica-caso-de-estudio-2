/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 2-LÓGICA DEL SISTEMA
 * La clase EDT hace de punto de entra al programa.
 * Se utiliza un soporte para la información, la entrada/salida
 * de información y el parser para procesar la entrada.
 */

public class EDT {

    private CharWindow w;
    protected Input input;
    protected Parser normalParser;
    protected Parser insertParser;
    protected Parser parser;
    protected Output out;
    public EDT(){}

   /**
    * @param width anchura establecida
    * @param height altura establecida
    * @param i carácter que leemos
    * @param o resultado que mostramos
    */
   public void setup(int width,int height, Input i, Output o){
       this.out=o;
       this.w=new CharWindow(width,height,' ',o);
       this.setupInput(i);
   }

   public void run(){
       char c = input.get();
       while(c!=0){
           parser.exe(c);
           c=input.get();
       }
       this.print();
   }

    /**
     * @return obtenemos el char dentro de la ventana
     */
   public CharWindow getWindow(){
       return this.w;
   }


   public void print(){
       this.out.print(this);
   }
    /**
     * @param i el carácter que obtenemos del input
     */
   public void setupInput(Input i){
       this.input=i;
   }

    /**
     * @param normalParser establecemos el NormalParser que contendrá las condiciones
     *                     de cada carácter que establezcamos en el input
     */
   public void setupNormalParser(Parser normalParser){
       this.normalParser = normalParser;
       this.parser=this.normalParser;
   }

    /**
     * @param insertParser ejecutará el parser dentro del edt
     */
   public void setupInsertParser(Parser insertParser){
       this.insertParser = insertParser;
   }

   public void setNormalMode(){
       this.parser=normalParser;
   }

   public void setInsertMode(){
       this.parser=insertParser;
   }

   public void next(){}

   public void prev(){}
}

