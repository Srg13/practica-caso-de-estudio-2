/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * Creación de la clase FileInput para poder escanear el contenido del archivo que incorporamos.
 */

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileInput extends Input{
    /**
     * @param url será el enlace a nuestro archivo.txt
     */
    public FileInput(String url) {
        super("");
        this.s=this.readFile(url);
    }

    /**
     * @param url leeremos el archivo
     * @return y convertiremos lo que haya dentro en un string que procesará nuestra clase input
     */
    private String readFile(String url){
        StringBuilder line = new StringBuilder();
        System.out.println(url);
        try {
            File file = new File(url);
            Scanner sc=new Scanner(file);    //file to be scanned
            while(sc.hasNextLine()) {
                //System.out.println(sc.nextLine());
                line.append(sc.nextLine());//returns the line that was skipped
            }
            sc.close();     //closes the scanner
        } catch(IOException e) {
            e.printStackTrace();
        }
        System.out.println(line.toString());
        return line.toString();
    }
}
