/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * Nuestro caso de estudio, EDT, como ya sabemos se compone de una serie
 * de vectores que contienen la información que maneja el editor. Para dar
 * soporte en nuestra aplicación extendemos la clase Data e implementamos
 * los métodos correspondientes.
 */

public class Array extends Data {

    protected Data[] content;
    protected int size;
    private Data def;
    public Array() {}

    /**
     * @param size será el tamaño de la array
     * @param def será para gestionar las operaciones que hará el dato que introduzcamos
     * @param o servirá para visualizar el dato
     */
    public Array(int size, Data def, Output o){
        this.content = new Data[size];
        this.setOutput(o);
        this.size=size;
        this.def=def;
        for(int i=0;i<this.content.length;i++){
            this.content[i]=this.def;
        }
    }

    /**
     * @param p para señalar la posición dentro de la matriz
     * @param v colocaremos el dato que se introduzca en la posición
     */

    @Override
    public void set(int p, Data v) {
        if(this.isLegal(p)){
            this.content[p] = v;
        }
    }

    /**
     * @param p obtendremos la posición y si es correcta se colocará en la array
     * @return si no es correcta, devolveremos una 'x' para indicar que esta mal
     */
    @Override
    public Data get(int p) {
        if(this.isLegal(p)) {
            return this.content[p];
        }else {
            System.out.println("Error, get");
            return new Char('x', this.out);
        }
    }

    /**
     * @param p señala la posición a borrar
     */
    @Override
    public void rem(int p) {
        if(this.isLegal(p)) {
            this.set(p,this.def);
        }
    }

    /**
     * @param p señala la posición a borrar y borra el carácter haciendo desplazamiento
     */
    @Override
    public void del(int p){
        if(this.isLegal(p)){
            for(int i=p;i<this.size-1;i++){
                this.set(i,this.get(i+1));
            }
            this.rem(this.size-1);
        }
    }

    /**
     * @param p la posición donde queremos insertar
     * @param v y el valor que queremos insertar
     */
    @Override
    public void ins(int p, Data v){
        if(this.isLegal(p)){
            for(int i=this.size-1; i>p;i--){
                this.set(i,this.get(i-1));
            }
            this.set(p,v);
        }
    }
    public void ins (int p){
        this.ins(p,this.def);
    }

    /**
     * @return para obtener el tamaño de la array
     */
    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public void print() {
        this.out.print(this);
    }

    /**
     * @return devolvemos la longitud de la array
     */
    public int length(){
        return content.length;
    }

    /**
     * @param p la condición que verifica que la posición sea correcta
     * @return retorna la posición dentro de la array
     */
    public boolean isLegal(int p){
        return(p>=0&&p<this.size);
    }

    /**
     * @param def para establecer lo definido
     */
    protected void setDef(Data def){ this.def=def;}
}
