/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 1.1-ALMACENAMIENTO Y MANIPULACIÓN DE LA INFORMACIÓN
 * Para representar la información a nivel de pantalla utilizaremos la clase Window.
 */
public class Window extends Matrix {
    public int x;
    public int y;

    /**
     * @param width para establecer la anchura de la ventana
     * @param height para establecer la altura de la ventana
     * @param def para establecer los datos que contendrá
     * @param o y cómo se mostrarán
     */
    public Window(int width,int height, Data def, Output o){
        super(width,height,def,o);
        this.x=0;
        this.y=0;
    }

    /**
     * @param x serán las filas
     * @param y serán las columnas
     * @return retornará la posición
     */
    public boolean setPos(int x, int y){
        return this.setPosX(x)&&this.setPosY(y);
    }

    /**
     * @param x será la posción dentro de la fila
     * @return devolverá falso si no está dentro de la fila
     */
    public boolean setPosX(int x){
        if(this.isLegalX(x)) {
            this.x=x;
            return true;
        }
        return false;
    }

    /**
     * @param y será la posición dentro de la columna
     * @return devolverá falso si no está dentro de la columna
     */
    public boolean setPosY(int y){
        if(this.isLegal(y)) {
            this.y=y;
            return true;
        }
        return false;
    }

    /**
     * @param x serán las filas
     * @param y serán las columnas
     * @return mirará que sean válidas las posiciones que le pasamos con el método
     */
    public boolean isLegalPos(int x, int y){
        return this.isLegalX(x)&&this.isLegalY(y);
    }

    /**
     * @return mirará que sea válido según los atributos de la clase
     */
    public boolean isLegalPos(){
        return this.isLegalPos(this.x,this.y);
    }

    /**
     * @param x serán las filas
     * @return mirará que sea válido dentro de las filas
     */
    public boolean isLegalX(int x) {
        if (this.get(this.y).isLegal(x)) {
            return true;
        }
        return false;
    }

    /**
     * @param y serán las columnas
     * @return mirará que sea válido dentro de las columnas
     */
    public boolean isLegalY(int y){
        if(super.isLegal(y)) {
            return true;
        }
        return false;
    }

    /**
     * @param v insertará el valor siempre que la posición sea válida
     */
    public void ins(Data v){
        if(this.isLegalX(this.x)) {
            super.ins(this.x, this.y, v);
            this.setPosX(this.x+1);
        }
    }

    public void ins(){
        this.nextLine();
        this.ins(this.y);
    }

    public void del(){
        this.del(this.y);
    }
    public void delPos(){
        this.get(this.y).del(this.x);
    }
    public boolean next(){
        return this.setPosX(this.x+1);
    }
    public boolean prev(){
        return this.setPosX(this.x-1);
    }
    public boolean nextLine(){
        return this.setPosY(this.y+1);
    }
    public boolean prevLine(){
        return this.setPosY(this.y-1);
    }
    public void first(){
        this.setPosX(0);
    }
    public void last(){
        this.setPosX(this.get(this.y).getSize()-1);
    }
    public void firstLine(){
        this.setPosY(0);
    }
    public void lastLine(){
        this.setPosY(this.getSize()-1);
    }
    public void print(){
        this.out.print(this);
    }

}
