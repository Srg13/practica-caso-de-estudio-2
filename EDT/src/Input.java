/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 3-ENTRADA DE INFORMACIÓN
 * La entrada de información al sistema se realiza mediante la clase input.
 */

public class Input {
    protected String s;
    private int p;

    /**
     * @param s será el argumento con el que introduciremos el string
     */
    public Input(String s){
        this.s=s;
        this.p=0;
    }

    /**
     * @return devolverá la información que queremos introducir
     */
    public char get(){
        char r = 0;
        if(p<s.length()) {
            r = s.charAt(p);
            this.p++;
        }
        return r;
    }
}
