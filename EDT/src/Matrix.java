/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * Esta clase servirá para gestionar la matriz que gestiona el texto de la ventana de EDT,
 * definimos la clase Matrix.
 */
public class Matrix extends Array{

    private int width;
    private Data def;

    /**
     * @param width la anchura de la matriz
     * @param height la altura de la matriz
     * @param def para definir el dato dentro de la matriz
     * @param o para mostrar el dato en la matriz
     */
    public Matrix(int width, int height, Data def, Output o) {

        this.size=height;
        this.width=width;
        this.def=def;
        this.setOutput(o);
        this.content=new Data[height];
        this.setDef(new Array(width,def,this.out));
        for(int i=0;i<height;i++){
            this.content[i]=new Array(width,def,this.out);
        }
    }

    /**
     * @param x para insertar en la fila
     * @param y para insertar en la columna
     * @param v para decir qué dato insertar
     */
    public void ins(int x, int y, Data v){
        this.get(y).ins(x,v);
    }

    /**
     * @param x la fila dónde se encuentra el dato a borrar
     * @param y la columna dónde se encuentra el dato a borrar
     */
    public void del(int x,int y){
        this.get(y).del(x);
    }

    /**
     * @param y cuando se introduce el texto se crea una nueva array
     */
    public void ins(int y){
        this.ins(y,new Array(this.width, this.def, this.out));
    }

    @Override
    public void print() {
        this.out.print(this);
    }

}
