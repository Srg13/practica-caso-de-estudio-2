/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 4.1-PROCESAMIENTO DE COMANDOS.
 * Para el procesamiento se realiza una clase genérica que
 * permitirá intercambiar el parser durante la ejecución del programa.
 */


public abstract class Parser {
    protected EDT edt;

    /**
     * @param edt define en el parser el editor de texto
     */
    public Parser(EDT edt){
        this.edt=edt;
    }

    /**
     * @param c para poder ejecutar el carácter en el parser
     */
    public abstract void exe(char c);
}
