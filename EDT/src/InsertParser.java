/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 4.3-PROCESAMIENTO DE COMANDOS.
 * La definición se ha dado de forma parcial, por lo tanto, la he completado.
 */
public class InsertParser extends Parser {
    /**
     * @param edt inserta en el edt el parser
     */
    public InsertParser(EDT edt){
        super(edt);
    }

    /**
     * @param c es el carácter a ejecutar dentro del parser, se establece el carácter
     *          para poner el editor en modo normal
     */
    public void exe(char c){
        if(c=='&'){
            this.edt.setNormalMode();
        }else{
            this.edt.getWindow().ins(c);
        }
    }
}
