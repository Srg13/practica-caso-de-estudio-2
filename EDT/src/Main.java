/**
 * @author Samantha Roldán García
 * @version v.1
 *
 *Compilación de las clases base del caso de estudio
 */
public class Main {
    /**
     * @param args son los argumentos que va a ejecutar el main
     */
    public static void main(String[] args){
        //execArray();
        //execMatrix();
        execFuncionCasoEstudio1_6();
        execFuncionCasoEstudio2_1();
        execFuncionCasoEstudio2_3();
    }

    private static void execFuncionCasoEstudio2_3() {
        Output o = new EDTOutput();
        FileInput fi = new FileInput("EDT/input.txt");
        EDT edt = new EDT();
        edt.setup(10,5,fi,o);
        NormalParser np=new NormalParser(edt);
        InsertParser ip=new InsertParser(edt);
        edt.setupNormalParser(np);
        edt.setupInsertParser(ip);
        edt.run();
    }

    private static void execFuncionCasoEstudio2_1() {
        Output o = new EDTOutput();
        Input i = new Input("");
        EDT edt = new EDT();
        edt.setup(10,5,i,o);
        NormalParser np=new NormalParser(edt);
        InsertParser ip=new InsertParser(edt);
        edt.setupNormalParser(np);
        edt.setupInsertParser(ip);
        edt.run();
    }

    static void execArray() {
        Output out = new DecoratedOuput();
        Array a = new Array(10,new Char('x',out),out);
        a.ins(3,new Char('h',out));
        a.ins(4,new Char('o',out));
        a.ins(5,new Char('l',out));
        a.ins(8,new Char('a',out));
        a.print();
        a.del(6);
        a.del(6);
        System.out.print("\n");
        a.print();
    }

    static void execMatrix() {
        Output out = new DecoratedOuput();
        Matrix m = new Matrix(10,5,new Char(' ',out),out);
        m.setOutput(out);
        m.ins(2,3,new Char('h',out));
        m.ins(3,3,new Char('o',out));
        m.ins(4,3,new Char('l',out));
        m.ins(6,3,new Char('a',out));
        m.del(5,3);
        m.del(2);
        m.print();
    }

    static void execFuncionCasoEstudio1_6(){
        Output o = new EDTOutput();
        Input i = new Input("jjllliHola&jhhhiMundo&kkkdd0xxxoˆˆˆˆvvvvv");
        EDT edt = new EDT();
        edt.setup(10,5,i,o);
        NormalParser np=new NormalParser(edt);
        InsertParser ip=new InsertParser(edt);
        edt.setupNormalParser(np);
        edt.setupInsertParser(ip);
        edt.run();
    }
}


