/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 1.2-ALMACENAMIENTO Y MANIPULACIÓN DE LA INFORMACIÓN
 */
public class CharWindow extends Window {
    /**
     *
     * @param width necesitaremos la longitud de la ventana
     * @param height y la altura de la ventana
     * @param def para introducir el dato que queremos
     * @param o y mostrarlo a través del output
     */
    public CharWindow (int width, int height, char def, Output o){
        super(width,height,new Char(def,o),o);
    }

    /**
     * @param c insertaremos el char y lo mostraremos
     */
    public void ins(char c){
        super.ins(new Char(c,this.out));
    }
}
