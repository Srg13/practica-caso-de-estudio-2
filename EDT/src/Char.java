/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * El componente básico de nuestro caso de estudio son los caracteres que
 * componen el texto. Para darles soporte implementamos la clase Char.
 */
public class Char extends Array {

    protected char content;

    /**
     * @param c es el caracter que introducimos
     * @param o será el que nos permita visualizar el carácter
     */
    public Char(char c,Output o){
        super();
        this.setOutput(o);
        this.set(c);
    }

    /**
     * @param c estableceremos en el contenido de la array el carácter
     */
    public void set(char c){
        this.content=c;
    }

    @Override
    public void print() {
        this.out.print(this);
    }

}
