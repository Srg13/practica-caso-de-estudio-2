/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * Esta clase ha sido creada para dar salida al programa a partir del Output.
 */
public class DecoratedOuput extends Output {
    /**
     * @param c impimimos el carácter que introducimos en la array
     */
    @Override
    public void print(Char c) {
        System.out.print(c.content);
    }

    /**
     * @param w usaremos la variable para imprimir en ventana
     */
    public void print(Window w){}

    /**
     * @param edt lo usaremos para imprimir el editor
     */
    public void print(EDT edt){}

    /**
     * @param a este argumento servirá para iterar el bucle hasta el tamaño que indiquemos en la array
     *          y así imprimir el cuerpo del editor
     */
    @Override
    public void print(Array a) {
        System.out.print("|");
        for (int i = 0; i < a.length(); i++) {
            a.get(i).print();
        }
        System.out.print("|");
    }

    /**
     * @param m este argumento servirá para iterar el bucle hasta el tamaño que le indiquemos a la matriz
     *          y así imprimir el cabezal y pie del editor
     */
    @Override
    public void print(Matrix m) {
        System.out.println();
        System.out.println(" ---------- ");
        for(int i=0;i<m.content.length;i++){
            m.content[i].print();
            System.out.println();
        }
        System.out.println(" ---------- ");
    }

}
