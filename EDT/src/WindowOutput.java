/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 5.1-SALIDA DE INFORMACIÓN.
 * La salid del sistema se produce utilizando las clases
 * base que extienden a la clase Output. Se definen dos nuevas salidas,
 * una para la clase Window y otro para la clase EDT.
 */
public class WindowOutput extends DecoratedOuput {
    /**
     * @param w usaremos la variable para imprimir en ventana
     */
    public void print(Window w){
        System.out.println("["+w.x+"-"+w.y+"]");
        super.print((Matrix)w);
    }
}
