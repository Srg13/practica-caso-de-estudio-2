/***
 * @author Samantha Roldán García
 * @version v.1
 *
 * Para visualitzar la información se proporciona la clase abstracta
 * Output que nos permitirá aplicar salidas a nuestra información.
 */
public abstract class Output {
    /**
     * @param c imprimirá el carácter
     */
    public abstract void print(Char c);
    public abstract void print(Array a);
    public abstract void print(Matrix m);
    public abstract void print(Window w);
    public abstract void print(EDT edt);

}
