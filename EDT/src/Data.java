/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * El manejo de los datos se realizará a través de especializaciones de la clase Data,
 * que proporciona los métodos correspondientes para gestionar datos.
 */

public abstract class Data {

    public Output out;

    /**
     * @param p obtendremos la posición y si es correcta se colocará en la array
     * @return si no es correcta, devolveremos una 'x' para indicar que esta mal
     */
    public abstract Data get(int p);

    /**
     * @param p se establece la posición
     * @param v y el valor del dato que introducimos
     */
    public abstract void set(int p, Data v);

    /**
     * @param p señala la posición a borrar
     */
    public abstract void rem(int p);

    /**
     * @param p señala la posición a borrar y borra el carácter haciendo desplazamiento
     */
    public abstract void del(int p);

    /**
     * @param p la posición donde queremos insertar
     * @param v y el valor que queremos insertar
     */
    public abstract void ins(int p, Data v);

    /**
     * @return para obtener el tamaño de la array
     */
    public abstract int getSize();

    /**
     * @param o para mostrar la salida
     */
    public void setOutput(Output o){
        this.out=o;
    }
    public abstract void print();

    /**
     * @param p la condición que verifica que la posición sea correcta
     * @return retorna la posición dentro de la array
     */
    public abstract boolean isLegal(int p);

}
