/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 4.2-PROCESAMIENTO DE COMANDOS.
 * La definición se ha dado de forma parcial, por lo tanto, la he completado.
 */
public class NormalParser extends Parser{
    /**
     * @param edt es el constructor de la herencia
     */
    public NormalParser(EDT edt){
        super(edt);
    }

    /**
     * @param c ejecutará en los carácteres especiales los casos asignados
     */
    public void exe(char c){
        switch (c){
            case '0':
                this.edt.getWindow().first();
                break;

            case 'd':
                this.edt.getWindow().del();
                break;

            case 'g':
                this.edt.getWindow().setPosY(0);
                break;

            case 'G':
                this.edt.getWindow().setPosY(this.edt.getWindow().getSize()-1);
                break;

            case 'h':
                this.edt.getWindow().prev();
                break;

            case 'i':
                this.edt.setInsertMode();
                break;

            case 'j':
                this.edt.getWindow().nextLine();
                break;

            case 'k':
                this.edt.getWindow().prevLine();
                break;

            case 'l':
                this.edt.getWindow().next();
                break;

            case 'o':
                this.edt.getWindow().ins();
                this.edt.getWindow().first();
                this.edt.setInsertMode();
                break;

            case 'x':
                this.edt.getWindow().delPos();
                break;

            case 'X':
                if(this.edt.getWindow().x != 0){
                    this.edt.getWindow().prev();
                    this.edt.getWindow().delPos();
                }
                break;

            case '$':
                this.edt.getWindow().last();
                break;
        }
    }
}
