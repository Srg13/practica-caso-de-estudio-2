/***
 * @author Samantha Roldán García
 * @version v.1
 *
 * Implementamos la clase Output para tener una clase concreta que nos permita
 * visualizar la información, teniendo como resultado la siguiente clase
 */
public class SimpleOutput extends Output {
    /**
     * @param c imprimirá el carácter
     */
    @Override
    public void print(Char c) {
        System.out.print(c.content);
    }

    /**
     * @param w imprimirá la ventana
     */
    public void print(Window w){}

    /**
     * @param edt imprimirá el editor de texto
     */
    public void print(EDT edt){}

    @Override
    public void print(Array a) {
        for (int i = 0; i < a.length(); i++) {
            a.get(i).print();
        }
    }

    @Override
    public void print(Matrix m) {
        System.out.println();
        for(int i=0;i<m.content.length;i++){
            m.content[i].print();
            System.out.println();
        }
    }
}
