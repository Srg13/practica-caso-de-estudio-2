/**
 * @author Samantha Roldán García
 * @version v.1
 *
 * 5.2-SALIDA DE INFORMACIÓN.
 * La salid del sistema se produce utilizando las clases
 * base que extienden a la clase Output. Se definen dos nuevas salidas,
 * una para la clase Window y otro para la clase EDT.
 */
public class EDTOutput extends WindowOutput {
    /**
     * @param edt lo usaremos para imprimir el editor
     */
    public void print(EDT edt){
        System.out.print("EDT ");
        super.print(edt.getWindow());
    }
}
